<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {return;}

// Déclaration des blocs Z
$GLOBALS['z_blocs'] = array('content', 'head', 'head_js', 'header', 'footer');
