# Changelog

## Unreleased

### Fix

- Réparer l'affichage des `input` type `radio`

## 0.4.6 - 2023-10-12

### Fix

- #11 Retrait d'une div non fermée inutile dans le pied de page
- #10 Permettre d'utiliser l'inclure des documents sur les événements

## 0.4.5 - 2023-09-13

### Fix

- #9 Item de langue <:html5up_telephasic:voir_tout:> manquant

### Changed

- Passage en stable

## 0.4.4 - 2023-09-07

### Added

- #8 Styles des paginations
- #6 Permettre d'utiliser des compositions sur les rubriques

## 0.4.2 - 2023-08-24

### Added

- Ajout des 2 modèles `<bouton>` et `<icone>`

### Fix

- Une hauteur minimum pour le bandeau d'entête afin que l'image soit visible sur petit écran
- Le thème ne gère pas le plugin Identité extra donc pas besoin de tester sa présence dans le pied de page

## 0.4.1 - 2023-07-23

### Changed

- Compatibilité SPIP 4.1+
- Ajout d'un `CHANGELOG.md` et mise à jour du `README.md`
- Passage en test
