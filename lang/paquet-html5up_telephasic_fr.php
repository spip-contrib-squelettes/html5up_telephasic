<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'html5up_telephasic_description' => 'Adaptation pour SPIP du squelette «Telephasic» de html5up https://html5up.net/telephasic',
	'html5up_telephasic_nom' => 'Html5up «Telephasic»',
	'html5up_telephasic_slogan' => 'Squelette responsive «Telephasic» de HTML5UP',
);
